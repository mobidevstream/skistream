import UIKit

class UIRecordButton: UIButton {

    let duration = 0.2
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    
    func setup() {
        setRunning(false)
        //addTarget(self, action: #selector(pressed), for: .touchUpInside)
        
    }
    
    func setRunning(_ running: Bool) {
        if running{
            set(title: "Stop", color: UIColor.red())
        } else {
            set(title: "Start", color: UIColor.green())
        }
    }
    
    
    func set(title: String, color: UIColor){
        DispatchQueue.main.async { [weak self] in
            self!.setTitle(title, for: .normal)
            UIView.animate(withDuration: self!.duration) {
                self!.backgroundColor = color
            }
        }
    }
}

import UIKit
import AVFoundation
import ARKit
import CoreLocation
import CoreMotion

class ViewController: UIViewController, AVCapturePhotoCaptureDelegate, ARSCNViewDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    @IBOutlet weak var startBtn: UIRecordButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var bufferSizeLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    private let locationManager = CLLocationManager()
    private var audioCaptureSession = AVCaptureSession()
    private var videoCaptureSession = AVCaptureSession()
    private var previewLayer = AVCaptureVideoPreviewLayer()
    private var clock = Clock()
    private var timerArray: [Timer] = []
    private var running: Bool = false
    private var buffer = Buffer()
    private var window = Proto_Window()
    private var desktop = ""
    private let frequency = 0.5
    private var sensorError: [String] = []
    private var sensors = 0
    private var sendWindow: SendWindowOperation?
    private let operationQueue = OperationQueue()
    private let videoOutput = AVCapturePhotoOutput()
    private let audioOutput = AVCaptureAudioDataOutput()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: SettingsEnum.video.key) ||
            UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key) {
            videoCaptureSession.stopRunning()
            videoCaptureSession = AVCaptureSession()
            let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                      for: .video,
                                                      position: .back)
            videoCaptureSession.beginConfiguration()
            guard
                let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice!),
                videoCaptureSession.canAddInput(videoDeviceInput)
                else { return }
            videoCaptureSession.addInput(videoDeviceInput)
            videoCaptureSession.addOutput(videoOutput)
            videoCaptureSession.commitConfiguration()
            previewLayer = AVCaptureVideoPreviewLayer(session: videoCaptureSession)
            previewLayer.videoGravity = .resizeAspectFill
            videoView.layer.insertSublayer(previewLayer, at: 0)

            if SettingsEnum.pointCloud.isAvailable &&
                UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key){
                sceneView.debugOptions = [.showWorldOrigin, .showFeaturePoints]
                sceneView.session.run(ARWorldTrackingConfiguration())
                sceneView.delegate = self
                sceneView.scene.background.contents = videoDevice
            }
        }
        if UserDefaults.standard.bool(forKey: SettingsEnum.audio.key) {
            audioCaptureSession.stopRunning()
            let audioDevice = AVCaptureDevice.default(.builtInMicrophone,
                                                      for: .audio,
                                                      position: .unspecified)
            audioCaptureSession.beginConfiguration()
            guard
                let audioDeviceInput = try? AVCaptureDeviceInput(device: audioDevice!),
                audioCaptureSession.canAddInput(audioDeviceInput)
                else { return }
            audioOutput.setSampleBufferDelegate(self, queue: .main)
            audioCaptureSession.addInput(audioDeviceInput)
            audioCaptureSession.addOutput(audioOutput)
            audioCaptureSession.commitConfiguration()
        }

        reset()
    }

    override func viewWillAppear(_ animated: Bool) {
        let scene = UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key)
        let video = UserDefaults.standard.bool(forKey: SettingsEnum.video.key)

        if !video && !scene {
            sceneView.isHidden = true
            videoView.isHidden = true
        } else if video && !scene {
            //Video only
            sceneView.isHidden = true
            videoView.isHidden = false
            videoCaptureSession.startRunning()
        } else if !video && scene {
            //Scene only
            sceneView.isHidden = false
            videoView.isHidden = true
        } else if  video && scene {
            sceneView.isHidden = false
            videoView.isHidden = true
            videoCaptureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: SettingsEnum.video.key) ||
            UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key) {
            videoCaptureSession.stopRunning()
        }
    }

    override func viewDidLayoutSubviews() {
        // Video frame position
        var f = self.videoView.frame
        f = f.offsetBy(dx: 0, dy: -70)
        self.previewLayer.frame = f
    }

    override func viewWillLayoutSubviews() {
        // Video orientation
        guard previewLayer.connection != nil else {return}
        let orientation = UIDevice.current.orientation
        switch (orientation) {
            case .portrait:
                previewLayer.connection!.videoOrientation = .portrait
            case .portraitUpsideDown:
                previewLayer.connection!.videoOrientation = .portraitUpsideDown
            case .landscapeLeft:
                previewLayer.connection!.videoOrientation = .landscapeRight
            case .landscapeRight:
                previewLayer.connection!.videoOrientation = .landscapeLeft
            default:
                return
        }
    }

    // Capture audio
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        var audio = Proto_Audio()
        if UserDefaults.standard.bool(forKey: SettingsEnum.audio.key){
            audio.timestamp = Date().millisecondsSince1970
        }
        if !self.window.audio.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
            self.window.audio.removeAll()
        }


        var audioBufferList = AudioBufferList()
        var blockBuffer : CMBlockBuffer?
        let audioBuffer = CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, bufferListSizeNeededOut: nil, bufferListOut: &audioBufferList, bufferListSize: MemoryLayout<AudioBufferList>.size, blockBufferAllocator: nil, blockBufferMemoryAllocator: nil, flags: 0, blockBufferOut: &blockBuffer)
        let buffers = UnsafeBufferPointer<AudioBuffer>(start: &audioBufferList.mBuffers, count: Int(audioBufferList.mNumberBuffers))

        var data = Data()
        for audioBuffer in buffers {
            let frame = audioBuffer.mData?.assumingMemoryBound(to: UInt8.self)
            data.append(frame!, count: Int(audioBuffer.mDataByteSize))
        }

        audio.audio = data
        self.window.audio.append(audio)
    }


    // Capture video
    func photoOutput(_ output: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photo: AVCapturePhoto,
                     error: Swift.Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        let imageData = photo.fileDataRepresentation()

        var video = Proto_Video()
        if UserDefaults.standard.bool(forKey: SettingsEnum.video.key){
            video.timestamp = Date().millisecondsSince1970
        }
        if imageData != nil {
            video.video = imageData!
        }
        if !self.window.video.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
            self.window.video.removeAll()
        }
        self.window.video.append(video)
    }

    @IBAction func startBtnTap(_ sender: Any) {
        if !running {
            let desktopAddress = UserDefaults.standard.string(forKey: SettingsEnum.desktopAddress.key)
            let desktopPort = UserDefaults.standard.string(forKey: SettingsEnum.desktopPort.key)

            guard desktopAddress != nil && desktopAddress != "" else {
                Alert.present(self, title: "Error", message: "Desktop address", buttonTitle: "OK")
                return
            }
            guard desktopPort != nil && desktopPort != "" else {
                Alert.present(self, title: "Error", message: "Desktop port", buttonTitle: "OK")
                return
            }
            if desktopAddress!.hasPrefix("localhost") || desktopAddress!.hasPrefix("127.0.0.1") || desktopAddress!.hasPrefix("192.168"){
                desktop = "http://\(desktopAddress!):\(desktopPort!)/stream"
            } else {
                desktop = "https://\(desktopAddress!):\(desktopPort!)/stream"
            }
            sendWindow = SendWindowOperation(desktop: desktop, buffer: buffer)
            start()
        } else {
            reset()
        }
    }

    @objc func incrementTimer(){
        clock.increment()
        label()
    }
    
    func label(){
        DispatchQueue.main.async { [weak self] in
            self?.timeLabel.text = self!.clock.string()
            self?.bufferSizeLabel.text = "Buffer size: \(self!.buffer.count)"
            self?.speedLabel.text = String(format: "Speed: %.2f MB/sec", self!.buffer.speed/1000)
        }
    }
    
    func start(){
        let group = DispatchGroup()
        
        if UserDefaults.standard.bool(forKey: SettingsEnum.audio.key){
            switch AVCaptureDevice.authorizationStatus(for: .audio) {
                case .authorized:
                    break
                case .denied, .restricted:
                    Alert.present(self, title: "Microphone permission needed", buttonTitle: "OK")
                case .notDetermined:
                    group.enter()
                    AVAudioSession.sharedInstance().requestRecordPermission({(success: Bool) in
                        if success {
                            group.leave()
                        }
                    })
                @unknown default:
                    fatalError()
            }
        }
        if UserDefaults.standard.bool(forKey: SettingsEnum.video.key) ||
            UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key){
            switch AVCaptureDevice.authorizationStatus(for: .video) {
                case .authorized:
                    break
                case .denied, .restricted:
                    Alert.present(self, title: "Camera permission needed", buttonTitle: "OK")
                case .notDetermined:
                    group.enter()
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: {success in
                        if success {
                            group.leave()
                        }
                    })
                @unknown default:
                    fatalError()
            }
        }
        if UserDefaults.standard.bool(forKey: SettingsEnum.position.key){
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                    case .authorizedAlways, .authorizedWhenInUse:
                        break
                    case .denied, .restricted:
                        Alert.present(self, title: "Position permission needed", buttonTitle: "OK")
                    case .notDetermined:
                        locationManager.requestAlwaysAuthorization()
                    @unknown default:
                        fatalError()
                }
            } else {
                Alert.present(self, title: "Turn on position", buttonTitle: "OK")
            }
        }
        
        
        group.notify(queue: .main) {
            
            var ok = true
            if UserDefaults.standard.bool(forKey: SettingsEnum.audio.key){
                ok = ok && AVCaptureDevice.authorizationStatus(for: .audio) == .authorized
            }
            if UserDefaults.standard.bool(forKey: SettingsEnum.video.key){
                ok = ok && AVCaptureDevice.authorizationStatus(for: .video) == .authorized
            }
            if UserDefaults.standard.bool(forKey: SettingsEnum.position.key){
                ok = ok && (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
            }
            
            guard ok else {return}
            
            //Audio
            if UserDefaults.standard.bool(forKey: SettingsEnum.audio.key){
                if SettingsEnum.audio.isAvailable {
                    self.sensors += 1
                    self.audioCaptureSession.startRunning()
                    
                } else {
                    self.sensorError.append(SettingsEnum.audio.title)
                }
            }
            //Video
            if UserDefaults.standard.bool(forKey: SettingsEnum.video.key){
                if SettingsEnum.video.isAvailable {
                    self.sensors += 1
                    let interval = self.frequency
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                self.videoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
                            })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.video.title)
                }
            }
            //Accelerometer
            if UserDefaults.standard.bool(forKey: SettingsEnum.accelerometer.key) {
                if SettingsEnum.accelerometer.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.accelerometerFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.accelerometerFrequency.key)].value
                    
                    let motion = CMMotionManager()
                    motion.accelerometerUpdateInterval = interval
                    motion.startAccelerometerUpdates()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var accelerometer = Proto_Accelerometer()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    accelerometer.timestamp = Date().millisecondsSince1970
                                }
                                
                                if let data = motion.accelerometerData {
                                    accelerometer.x = data.acceleration.x
                                    accelerometer.y = data.acceleration.y
                                    accelerometer.z = data.acceleration.z
                                }
                                
                                if !self.window.accelerometer.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.accelerometer.removeAll()
                                }
                                self.window.accelerometer.append(accelerometer)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.accelerometer.title)
                }
            }
            //Accelerometer raw
            if UserDefaults.standard.bool(forKey: SettingsEnum.accelerometerRaw.key) {
                if SettingsEnum.accelerometerRaw.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.accelerometerRawFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.accelerometerRawFrequency.key)].value
                    
                    let motion = CMMotionManager()
                    motion.accelerometerUpdateInterval = interval
                    motion.startAccelerometerUpdates()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var accelerometerRaw = Proto_AccelerometerRaw()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    accelerometerRaw.timestamp = Date().millisecondsSince1970
                                }
                                
                                if let data = motion.accelerometerData {
                                    accelerometerRaw.x = data.acceleration.x
                                    accelerometerRaw.y = data.acceleration.y
                                    accelerometerRaw.z = data.acceleration.z
                                }
                                
                                if !self.window.accelerometerRaw.isEmpty &&  !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.accelerometerRaw.removeAll()
                                }
                                self.window.accelerometerRaw.append(accelerometerRaw)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.accelerometerRaw.title)
                }
            }
            //Position
            if UserDefaults.standard.bool(forKey: SettingsEnum.position.key) {
                if SettingsEnum.position.isAvailable {
                    self.sensors += 1
                    let interval = self.frequency
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var position = Proto_Position()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    position.timestamp = Date().millisecondsSince1970
                                }
                                
                                position.latitude = Double(self.locationManager.location?.coordinate.latitude ?? 0)
                                position.longitude = Double(self.locationManager.location?.coordinate.longitude ?? 0)
                                
                                if !self.window.position.isEmpty &&  !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.position.removeAll()
                                }
                                self.window.position.append(position)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.position.title)
                }
            }
            //Gyroscope
            if UserDefaults.standard.bool(forKey: SettingsEnum.gyroscope.key){
                if SettingsEnum.gyroscope.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.gyroscopeFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.gyroscopeFrequency.key)].value
                    
                    let motion = CMMotionManager()
                    motion.gyroUpdateInterval = interval
                    motion.startGyroUpdates()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var gyroscope = Proto_Gyroscope()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    gyroscope.timestamp = Date().millisecondsSince1970
                                }
                                
                                if let data = motion.gyroData {
                                    gyroscope.x = data.rotationRate.x
                                    gyroscope.y = data.rotationRate.y
                                    gyroscope.z = data.rotationRate.z
                                }
                                
                                if !self.window.gyroscope.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.gyroscope.removeAll()
                                }
                                self.window.gyroscope.append(gyroscope)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.gyroscope.title)
                }
            }
            //Gyroscope raw
            if UserDefaults.standard.bool(forKey: SettingsEnum.gyroscopeRaw.key){
                if SettingsEnum.gyroscopeRaw.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.gyroscopeFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.gyroscopeFrequency.key)].value
                    
                    let motion = CMMotionManager()
                    motion.gyroUpdateInterval = interval
                    motion.startGyroUpdates()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var gyroscopeRaw = Proto_GyroscopeRaw()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    gyroscopeRaw.timestamp = Date().millisecondsSince1970
                                }
                                
                                if let data = motion.gyroData {
                                    gyroscopeRaw.x = data.rotationRate.x
                                    gyroscopeRaw.y = data.rotationRate.y
                                    gyroscopeRaw.z = data.rotationRate.z
                                }
                                
                                if !self.window.gyroscopeRaw.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.gyroscopeRaw.removeAll()
                                }
                                self.window.gyroscopeRaw.append(gyroscopeRaw)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.gyroscopeRaw.title)
                }
            }
            //Magnetometer
            if UserDefaults.standard.bool(forKey: SettingsEnum.magnetometer.key){
                if SettingsEnum.magnetometer.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.magnetometerFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.magnetometerFrequency.key)].value
                    let manager = CMMotionManager()
                    manager.startMagnetometerUpdates()

                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var magnetometer = Proto_Magnetometer()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    magnetometer.timestamp = Date().millisecondsSince1970
                                }
                                magnetometer.x = manager.magnetometerData?.magneticField.x ?? 0
                                magnetometer.y = manager.magnetometerData?.magneticField.y ?? 0
                                magnetometer.z = manager.magnetometerData?.magneticField.z ?? 0
                                if !self.window.magnetometer.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.magnetometer.removeAll()
                                }
                                self.window.magnetometer.append(magnetometer)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.magnetometer.title)
                }
            }
            //Barometer
            if UserDefaults.standard.bool(forKey: SettingsEnum.barometer.key){
                if SettingsEnum.barometer.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.barometerFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.barometerFrequency.key)].value
                    let manager = CMAltimeter()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var barometer = Proto_Barometer()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    barometer.timestamp = Date().millisecondsSince1970
                                }

                                manager.startRelativeAltitudeUpdates(to: OperationQueue.main)
                                { (data, error) in
                                    barometer.pressure = data!.pressure as! Double
                                    if !self.window.barometer.isEmpty &&  !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                        self.window.barometer.removeAll()
                                    }
                                    self.window.barometer.append(barometer)
                                }

                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.barometer.title)
                }
            }
            //Activity
            if UserDefaults.standard.bool(forKey: SettingsEnum.activity.key){
                if SettingsEnum.activity.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.activityFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.activityFrequency.key)].value
                    
                    let manager = CMMotionActivityManager()
                    manager.stopActivityUpdates()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var activity = Proto_Activity()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    activity.timestamp = Date().millisecondsSince1970
                                }
                                
                                manager.queryActivityStarting(from: Date.init(),
                                                              to: Date.init(),
                                                              to: OperationQueue.main)
                                {(activityList, error) in
                                    var activityString = ""
                                    let a = activityList![0]
                                    if (a.stationary) {
                                        activityString = "Stationary"
                                    }
                                    else if (a.walking) {
                                        activityString = "Walking"
                                    }
                                    else if (a.running) {
                                        activityString = "Running"
                                    }
                                    else if (a.automotive) {
                                        activityString = "Automotive"
                                    }
                                    else if (a.cycling) {
                                        activityString = "Cycling"
                                    }
                                    else {
                                        activityString = "Unknown"
                                    }
                                    activity.name = activityString

                                    if !self.window.activity.isEmpty && !self.window.activity.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                        self.window.activity.removeAll()
                                    }
                                    self.window.activity.append(activity)
                                }
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.activity.title)
                }
            }
            //Point cloud
            if UserDefaults.standard.bool(forKey: SettingsEnum.pointCloud.key){
                if SettingsEnum.pointCloud.isAvailable {
                    self.sensors += 1
                    let interval = self.frequency
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                print("Point cloud")
                        })
                    )

                } else {
                    self.sensorError.append(SettingsEnum.pointCloud.title)
                }
            }
            //Step counter
            if UserDefaults.standard.bool(forKey: SettingsEnum.stepCounter.key){
                if SettingsEnum.stepCounter.isAvailable {
                    self.sensors += 1
                    let interval = self.frequency
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var stepCounter = Proto_StepCounter()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    stepCounter.timestamp = Date().millisecondsSince1970
                                }
                                stepCounter.steps = Int32(truncating: CMPedometerData().numberOfSteps)
                                if !self.window.stepCounter.isEmpty &&  !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.stepCounter.removeAll()
                                }
                                self.window.stepCounter.append(stepCounter)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.stepCounter.title)
                }
            }
            //Proximity
            if UserDefaults.standard.bool(forKey: SettingsEnum.proximity.key) {
                if SettingsEnum.proximity.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.proximityFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.proximityFrequency.key)].value
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var proximity = Proto_Proximity()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    proximity.timestamp = Date().millisecondsSince1970
                                }
                                proximity.proximity = UIDevice().proximityState
                                if !self.window.proximity.isEmpty &&  !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                    self.window.proximity.removeAll()
                                }
                                self.window.proximity.append(proximity)
                        })
                    )
                } else {
                    self.sensorError.append(SettingsEnum.proximity.title)
                }
            }
            //Altimeter
            if UserDefaults.standard.bool(forKey: SettingsEnum.altimeter.key){
                if SettingsEnum.altimeter.isAvailable {
                    self.sensors += 1
                    let interval = SettingsEnum.barometerFrequency.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.barometerFrequency.key)].value

                    let manager = CMAltimeter()
                    self.timerArray.append(
                        Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block:
                            { (timer) in
                                var altimeter = Proto_Altimeter()
                                if UserDefaults.standard.bool(forKey: SettingsEnum.sensorTimestamp.key){
                                    altimeter.timestamp = Date().millisecondsSince1970
                                }

                                manager.startRelativeAltitudeUpdates(to: OperationQueue.main)
                                { (data, error) in
                                    altimeter.altimeter = data!.relativeAltitude as! Double
                                    if !self.window.altimeter.isEmpty && !UserDefaults.standard.bool(forKey: SettingsEnum.sameType.key){
                                        self.window.altimeter.removeAll()
                                    }
                                    self.window.altimeter.append(altimeter)
                                }

                        })
                    )
                    
                } else {
                    self.sensorError.append(SettingsEnum.altimeter.title)
                }
            }
            //Sensor error
            if(self.sensorError.count > 0){
                self.sensorError.sort()
                var message = ""
                for s in self.sensorError{
                    message = message + s + "\n"
                }
                Alert.present(self, title: "Sensor error (\(self.sensorError.count))", message: message, buttonTitle: "OK",
                              handler: {(action: UIAlertAction!) in
                                self.run()
                })
                self.sensorError = []
                
            } else {
                self.run()
            }
            
        }
    }
    
    func run(){
        guard self.sensors > 0 else {return}
        running = true
        startBtn.setRunning(true)
        operationQueue.addOperation(sendWindow!)
        timerArray.append(Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.incrementTimer), userInfo: nil, repeats: true))
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard self != nil else {
                return
            }
            while self!.running {
                self!.window = Proto_Window()
                self!.window.timestamp = Date().millisecondsSince1970
                var send = false
                while(!send){
                    let index = UserDefaults.standard.integer(forKey: SettingsEnum.windowSize.key)
                    send = (Date().millisecondsSince1970 - self!.window.timestamp) > Int(SettingsEnum.windowSize.pickerOptionData![index].value)
                }
                self!.buffer.add(self!.window)
            }
        }
    }
    
    func reset() {
        running = false
        startBtn.setRunning(false)
        
        sendWindow?.cancel()
        for t in timerArray {
            t.invalidate()
        }
        
        clock = Clock()
        buffer = Buffer()
        sensors = 0
        sensorError = []
        timerArray = []

        audioCaptureSession.stopRunning()

        label()

    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "goToSettingsTableViewController"{
            if !running {
                return true
            } else {
                Alert.present(self, title: "Stop recording first", buttonTitle: "OK")
                return false
            }
        }
        return false
    }
}

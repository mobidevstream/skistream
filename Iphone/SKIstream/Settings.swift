import UIKit

struct Sensor {
    var isAvailable: (() -> Bool)

    init(isAvailable: @escaping (() -> Bool), options: [Setting] = []){
        self.isAvailable = isAvailable
    }
}

struct PickerOption {
    var value: Double
    var name: String
    init(_ value: Double, _ name: String){
        self.value = value
        self.name = name
    }
}

class Setting {
    var key: String
    var title: String
    var help: String
    var type: SettingType
    var placeholder: String?
    var textType: UITextContentType?
    var pickerOptionData: [PickerOption]?
    var options: [Setting]?
    var sensor: Sensor?

    init(settingEnum: SettingsEnum, type: SettingType, options: [Setting]? = nil, sensor: Sensor? = nil){
        self.key = settingEnum.key
        self.title = settingEnum.title
        self.help = settingEnum.help
        self.placeholder = settingEnum.placeholder
        self.pickerOptionData = settingEnum.pickerOptionData
        self.textType = settingEnum.textType
        self.type = type
        self.options = options
        self.sensor = sensor
    }

}

class Section {
    var title: String
    var settings: [Setting]

    init(section: SectionEnum, sort: Bool = false, settings: [Setting]){
        self.title = section.title
        if !sort {
            self.settings = settings
        }
        else {
            self.settings = settings.sorted(by: { $0.key < $1.key })
        }
    }
}

struct Settings {
    private var allSettings: [Section]

    init(){
        allSettings = [
            Section(
                section: .global,
                settings: [Setting(settingEnum: .desktopAddress,
                                   type: .text),
                           Setting(settingEnum: .desktopPort,
                                   type: .text),
                           Setting(settingEnum: .drop,
                                   type: .onoff),
                           Setting(settingEnum: .windowSize,
                                   type: .picker),
                           Setting(settingEnum: .sameType,
                                   type: .onoff),
                           Setting(settingEnum: .sensorTimestamp,
                                   type: .onoff)]
            ),
            Section(
                section: .sensors,
                sort: true,
                settings: [
                    Setting(settingEnum: .audio,
                            type: .sensor,
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .video,
                            type: .sensor,
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .pointCloud,
                            type: .sensor,
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .accelerometer,
                            type: .sensor,
                            options: [Setting(settingEnum: .accelerometerFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .accelerometerRaw,
                            type: .sensor,
                            options: [Setting(settingEnum: .accelerometerRawFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .gyroscope,
                            type: .sensor,
                            options: [Setting(settingEnum: .gyroscopeFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .gyroscopeRaw,
                            type: .sensor,
                            options: [Setting(settingEnum: .gyroscopeRawFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .magnetometer,
                            type: .sensor,
                            options: [Setting(settingEnum: .magnetometerFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .barometer,
                            type: .sensor,
                            options: [Setting(settingEnum: .barometerFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .stepCounter,
                            type: .sensor,
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .position,
                            type: .sensor,
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .activity,
                            type: .sensor,
                            options: [Setting(settingEnum: .activityFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .proximity,
                            type: .sensor,
                            options: [Setting(settingEnum: .proximityFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    ),
                    Setting(settingEnum: .altimeter,
                            type: .sensor,
                            options: [Setting(settingEnum: .altimeterFrequency, type: .picker)],
                            sensor: Sensor(isAvailable: {return false})
                    )
                ]
            )
        ]
    }

    func countSections() -> Int {
        return allSettings.count
    }

    func countSettings(section: Int) -> Int {
        return allSettings[section].settings.count
    }

    func countSettings() -> Int {
        var c = 0
        for section in stride(from: 0, to: allSettings.count, by: 1) {
            c += countSettings(section: section)
        }
        return c
    }

    func titleSection(section: Int) -> String? {
        let title = allSettings[section].title
        if  title != SectionEnum.sensors.title {
            return title
        } else {
            return title + " (\(countSettings(section: section)))"
        }
    }

    func getSetting(section: Int, row: Int, option: Int = -1) -> Setting {
        let setting = allSettings[section].settings[row]
        if (option < 0){
            return setting
        } else {
            return setting.options![option]
        }
    }

}

import UIKit

extension UIColor{
    
    static func green() -> UIColor{
        return UIColor(red: 0.0, green: 0.65, blue: 0.0, alpha: 1.0)
    }
    
    static func red() -> UIColor{
        return UIColor(red: 0.85, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    
    static func gray() -> UIColor{
        return UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    }
}

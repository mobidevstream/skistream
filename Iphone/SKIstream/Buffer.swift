import Foundation

class Buffer{

    private var buffer: [Proto_Window]
    private let queue = DispatchQueue(label: "queue")
    var speed: Double = 0.0

    var count:Int{
        return copy().buffer.count
    }

    init() {
        buffer = []

    }
    init(_ b: [Proto_Window]){
        buffer = b
    }

    func window(_ pos:Int)->Proto_Window{
        return copy().buffer[pos]
    }

    func copy()->Buffer{
        queue.sync {
            var b: Buffer
            b = Buffer(buffer)
            return b
        }
    }

    func add(_ window: Proto_Window){
        queue.sync {
            buffer.append(window)
        }

    }

    func remove(_ pos:Int)->Proto_Window{
        queue.sync {
            return buffer.remove(at: pos)
        }
    }

}

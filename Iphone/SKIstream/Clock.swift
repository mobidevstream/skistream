import Foundation

class Clock {
    var ms: Int
    var seconds: Int
    var minutes: Int
    
    init(){
        ms = 0
        seconds = 0
        minutes = 0
    }

    func increment(){
        if ms == 9 {
            if seconds == 59 {
                minutes += 1
                seconds = 0
                ms = 0
            } else {
                seconds += 1
                ms = 0
            }
        } else {
            ms += 1
        }
    }

    func string() -> String {
        return String(format: "%02d:%02d.%01d", minutes, seconds, ms)
    }

}

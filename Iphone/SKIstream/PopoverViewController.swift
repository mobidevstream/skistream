import UIKit

class PopoverViewController: UIViewController {

    
    @IBOutlet weak var helpText: UITextView!
    var helpTxt: String = ""
    
    @IBAction func popoverExit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        helpText.text = helpTxt
        
    }
    

}

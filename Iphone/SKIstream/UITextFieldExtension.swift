import UIKit

extension UITextField {
    func setup(type: UITextContentType) {
        self.textAlignment = .center
        self.borderStyle = .roundedRect
        self.clearButtonMode = .never
        self.spellCheckingType = .no
        self.keyboardAppearance = .default
        self.autocapitalizationType = .none
        self.textContentType = type
    }
}

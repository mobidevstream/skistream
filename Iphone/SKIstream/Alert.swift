import UIKit

struct Alert {
    static func present(_ viewController: UIViewController, title: String, message: String? = nil, buttonTitle: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: handler))
        viewController.present(alert, animated: true, completion: nil)
    }
}

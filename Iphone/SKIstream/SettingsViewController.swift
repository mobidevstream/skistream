import UIKit

class SettingsViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    private let cellIdentifier = "settingsCell"
    private let popoverIdentifier = "popoverVC"

    private let viewPicker = UIView()
    private var picker = UIPickerView()
    private var activeIndex = (-1, -1, -1)
    private var willActiveIndex = (-1, -1, -1)
    private var activeButton = UIButton()
    private var activeView = UIView()

    private let settings = Settings()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        pickerSetup()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return settings.countSections()
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return settings.titleSection(section: section)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.countSettings(section: section)

    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SettingsTableViewCell {
            for subview in cell.contentView.subviews {
                subview.removeFromSuperview()
            }

            let setting = settings.getSetting(section: indexPath[0], row: indexPath[1])

            uiSetting(view: cell.contentView, setting: setting)

            if setting.options != nil {
                let v = UIView()
                v.tag = -10
                enableView(view: v, enable: UserDefaults.standard.bool(forKey: setting.key))
                cell.contentView.addSubview(v)

                let height = CGFloat(setting.options!.count * 35)
                constraints(subview: v, view: cell.contentView,
                            top: 45, bottom: -20, leading: 20, trailing: -20, height: height )


                for i in stride(from: 0, to: setting.options!.count, by: 1) {
                    let o = setting.options![i]
                    uiSetting(view: v, setting: o, tag: i, offset: CGFloat(i * 35))
                }

            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }


    private func uiSetting(view: UIView, setting: Setting, tag: Int = -1, offset: CGFloat = 0){
        //Title
        let titleLbl = UILabel()
        titleLbl.text = setting.title
        titleLbl.sizeToFit()
        view.addSubview(titleLbl)
        constraints(subview: titleLbl, view: view,
                    top: 11 + offset, leading: 20)

        //Help
        let helpBtn = UIButton()
        helpBtn.setImage(UIImage(named: "HelpImage"), for: .normal)
        helpBtn.tag = tag
        helpBtn.addTarget(self, action: #selector(helpButtonTap), for: UIControl.Event.touchUpInside)
        view.addSubview(helpBtn)
        constraints(subview: helpBtn, view: titleLbl,
                    trailing: 30)
        constraints(subview: helpBtn, view: view,
                    top: 11 + offset)

        //Type
        switch setting.type {
            case .onoff, .sensor:
                let uiSwitch = UISwitch()
                uiSwitch.isOn = UserDefaults.standard.bool(forKey: setting.key)
                uiSwitch.tag = tag
                uiSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                view.addSubview(uiSwitch)
                constraints(subview: uiSwitch, view: view,
                            top: 6 + offset, trailing: -20)
            case .picker:
                let pickerBtn = UIButton(type: .system)
                let row = UserDefaults.standard.integer(forKey: setting.key)
                pickerBtn.setTitle(setting.pickerOptionData![row].name, for: .normal)
                pickerBtn.tag = tag
                pickerBtn.addTarget(self, action: #selector(pickerButtonTap), for: UIControl.Event.touchUpInside)
                view.addSubview(pickerBtn)
                constraints(subview: pickerBtn, view: view,
                            top: 6 + offset, trailing: -20)
            case .text:
                let textField = UITextField()
                textField.setup(type: setting.textType!)
                textField.placeholder = setting.placeholder
                textField.text = UserDefaults.standard.string(forKey: setting.key)
                textField.tag = tag
                textField.addTarget(self, action: #selector(textChanged), for: UIControl.Event.editingChanged)
                view.addSubview(textField)
                constraints(subview: textField, view: view,
                            top: 5 + offset, trailing: -20, width: 100)
        }
    }


    //number of columns
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    //number of rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if willActiveIndex != (-1, -1, -1) {
            let setting = settings.getSetting(section: willActiveIndex.0, row: willActiveIndex.1, option: willActiveIndex.2)
            return setting.pickerOptionData?.count ?? 0
        } else {
            return 0
        }
    }

    //name of the rows
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if willActiveIndex != (-1, -1, -1) {
            let setting = settings.getSetting(section: willActiveIndex.0, row: willActiveIndex.1, option: willActiveIndex.2)
            return setting.pickerOptionData?[row].name ?? ""
        } else {
            return ""
        }
    }


    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let setting = settings.getSetting(section: activeIndex.0, row: activeIndex.1, option: activeIndex.2)
        //set title of the button
        activeButton.setTitle(setting.pickerOptionData![row].name, for: .normal)
        //save selected value
        UserDefaults.standard.set(row, forKey: setting.key)
    }

    func pickerSetup(){

        let height: CGFloat = 220
        let pickerBarHeight: CGFloat = 44
        let pickerHeight: CGFloat = 200
        viewPicker.isHidden = true
        self.view.addSubview(viewPicker)
        //constraints settings of the picker
        viewPicker.translatesAutoresizingMaskIntoConstraints = false
        viewPicker.heightAnchor.constraint(equalToConstant: height).isActive = true
        viewPicker.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        viewPicker.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        viewPicker.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true

        //Picker bar
        let pickerBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.safeAreaLayoutGuide.layoutFrame.width, height: pickerBarHeight))
        pickerBar.backgroundColor = .gray
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closePicker))
        pickerBar.setItems([flexibleSpace, doneButton], animated: false)
        viewPicker.addSubview(pickerBar)

        //Picker
        picker = UIPickerView(frame: CGRect(x: 0, y: pickerBarHeight, width: self.view.safeAreaLayoutGuide.layoutFrame.width, height: pickerHeight))
        picker.tag = -1
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.gray()
        viewPicker.addSubview(picker)

        //Bottom
        let bottom = UIView(frame: CGRect(x: 0, y: pickerBarHeight + pickerHeight, width: self.view.safeAreaLayoutGuide.layoutFrame.width, height: height))
        bottom.backgroundColor = UIColor.gray()
        viewPicker.addSubview(bottom)

    }

    @objc func textChanged(sender: UITextField) {
        let setting = getSetting(sender: sender)
        UserDefaults.standard.set(sender.text, forKey: setting.key)
    }

    @objc func switchChanged(sender: UISwitch) {
        let index = getIndex(sender: sender)
        let setting = getSetting(sender: sender)
        UserDefaults.standard.set(sender.isOn, forKey: setting.key)

        if setting.options != nil {
            let cell = tableView.cellForRow(at: IndexPath(indexes: [index.0, index.1]))
            for subview in cell!.contentView.subviews {
                if subview.tag == -10 {
                    enableView(view: subview, enable: sender.isOn)
                }
            }
        }
    }

    //tapped picker button
    @objc func pickerButtonTap(sender: UIButton) {
        let index = getIndex(sender: sender)
        let setting = getSetting(sender: sender)
        willActiveIndex = index

        if willActiveIndex != activeIndex || viewPicker.isHidden {
            picker.tag = index.2
            picker.reloadAllComponents()
            let row = UserDefaults.standard.integer(forKey: setting.key)
            picker.selectRow(row, inComponent: 0, animated: false)
            viewPicker.isHidden = false
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
            viewPicker.isHidden = true
        }

        activeIndex = willActiveIndex
        activeButton = sender
    }
    //tapped close button
    @objc func closePicker(v: UIBarButtonItem) {
        tableView.isScrollEnabled = true
        viewPicker.isHidden = true
    }

    func enableView(view: UIView, enable: Bool) {
        view.isUserInteractionEnabled = enable
        if enable {
            view.alpha = 1.0
        } else {
            view.alpha = 0.3
        }
    }

    //tapped help button
    @objc func helpButtonTap(sender: UIButton) {
        let setting = getSetting(sender: sender)

        let popoverVC = self.storyboard?.instantiateViewController(withIdentifier: popoverIdentifier) as! PopoverViewController
        popoverVC.modalPresentationStyle = .popover
        popoverVC.helpTxt = setting.help
        let popoverPresentationVC = popoverVC.popoverPresentationController
        popoverPresentationVC?.permittedArrowDirections = .up
        popoverPresentationVC?.sourceView = sender
        popoverPresentationVC?.sourceRect = sender.bounds
        present(popoverVC, animated: true, completion: nil)
    }

    private func constraints(subview: UIView, view: UIView,
                             top: CGFloat? = nil, bottom: CGFloat? = nil, leading: CGFloat? = nil, trailing: CGFloat? = nil, height: CGFloat? = nil, width: CGFloat? = nil){
        subview.translatesAutoresizingMaskIntoConstraints = false

        if top != nil {
            subview.topAnchor.constraint(equalTo: view.topAnchor, constant: top!).isActive = true
        }
        if bottom != nil {
            subview.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottom!).isActive = true
        }
        if leading != nil {
            subview.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leading!).isActive = true
        }
        if trailing != nil {
            subview.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailing!).isActive = true
        }
        if height != nil {
            subview.heightAnchor.constraint(equalToConstant: height!).isActive = true
        }
        if width != nil {
            subview.widthAnchor.constraint(equalToConstant: width!).isActive = true
        }

    }

    private func getIndex(sender: UIView) -> (Int, Int, Int) {
        let touchPoint = sender.convert(CGPoint.zero, to: self.tableView)
        let clickedButtonIndexPath = self.tableView.indexPathForRow(at: touchPoint)
        if clickedButtonIndexPath != nil {
            return (Int(clickedButtonIndexPath!.section), Int(clickedButtonIndexPath!.row), sender.tag)
        } else {
            return (0, 0, -1)
        }
    }

    private func getSetting(sender: UIView) -> Setting {
        let index = getIndex(sender: sender)
        return settings.getSetting(section: index.0, row: index.1, option: index.2)
    }
    
}


//popover help
extension SettingsViewController: UIPopoverPresentationControllerDelegate {

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

import UIKit

class SendWindowOperation: Operation {
    private var desktop: String
    private var buffer: Buffer
    private var windowSize: UInt32 = 0

    init(desktop: String, buffer: Buffer){
        self.desktop = desktop
        self.buffer = buffer
        windowSize = UInt32(SettingsEnum.windowSize.pickerOptionData![UserDefaults.standard.integer(forKey: SettingsEnum.windowSize.key)].value)
    }

    override public func main(){
        while !self.isCancelled {
            sleep(windowSize/2000)
            if buffer.count > 0 {
                let session = URLSession.shared
                let url = URL(string: desktop)

                var request = URLRequest(url: url!)
                request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
                request.setValue("text/plain", forHTTPHeaderField: "Accept")
                request.httpMethod = "POST"
                let serializedData  = try? buffer.window(0).serializedData()
                let ts = buffer.window(0).timestamp

                _ = buffer.remove(0)

                request.httpBody = serializedData

                let task = session.dataTask(with: request) { (data, response, error) in
                    guard error == nil else {
                        self.buffer.speed = 0.0
                        if !UserDefaults.standard.bool(forKey: SettingsEnum.drop.key){
                            let w = try? Proto_Window(serializedData: serializedData!)
                            self.buffer.add(w!)
                        }
                        return
                    }

                    guard serializedData != nil else{
                        return
                    }
                    let length  = Double(serializedData!.count)
                    let elapsed = Double(Date().timeIntervalSince(Date(milliseconds: ts)))
                    self.buffer.speed = length/elapsed

                }
                task.resume()
            }
        }
    }
}


import UIKit
import AVFoundation
import CoreLocation
import CoreMotion
import ARKit

enum SectionEnum {
    case global
    case sensors

    var title: String {
        switch self {
            case .global: return "Global"
            case .sensors: return "Sensors"
        }
    }
}

enum SettingType {
    case onoff
    case picker
    case text
    case sensor
}

enum SettingsEnum {
    case desktopAddress
    case desktopPort
    case drop
    case windowSize
    case sameType
    case sensorTimestamp
    case audio
    case video
    case pointCloud
    case accelerometer
    case gyroscope
    case magnetometer
    case barometer
    case stepCounter
    case position
    case activity
    case videoQuality
    case accelerometerFrequency
    case gyroscopeFrequency
    case magnetometerFrequency
    case barometerFrequency
    case activityFrequency
    case proximity
    case proximityFrequency
    case altimeter
    case altimeterFrequency
    case accelerometerRaw
    case accelerometerRawFrequency
    case gyroscopeRaw
    case gyroscopeRawFrequency

    var key: String {
        switch self {
            case .desktopAddress: return "desktopAddress"
            case .desktopPort: return "desktopPort"
            case .drop: return "drop"
            case .windowSize: return "windowSize"
            case .sameType: return "sameType"
            case .sensorTimestamp: return "sensorTimestamp"
            case .audio: return "audio"
            case .video: return "video"
            case .pointCloud: return "pointCloud"
            case .accelerometer: return "accelerometer"
            case .gyroscope: return "gyroscope"
            case .magnetometer: return "magnetometer"
            case .barometer: return "barometer"
            case .stepCounter: return "stepCounter"
            case .position: return "position"
            case .activity: return "activity"
            case .videoQuality: return "videoQuality"
            case .accelerometerFrequency: return "accelerometerFrequency"
            case .gyroscopeFrequency: return "gyroscopeFrequency"
            case .magnetometerFrequency: return "magnetometerFrequency"
            case .barometerFrequency: return "barometerFrequency"
            case .activityFrequency: return "activityFrequency"
            case .proximity: return "proximity"
            case .altimeter: return "altimeter"
            case .proximityFrequency: return "proximityFrequency"
            case .altimeterFrequency: return "altimeterFrequency"
            case .accelerometerRaw: return "accelerometerRaw"
            case .accelerometerRawFrequency: return "accelerometerRawFrequency"
            case .gyroscopeRaw: return "gyroscopeRaw"
            case .gyroscopeRawFrequency: return "gyroscopeRawFrequency"
        }
    }
    var title: String {
        switch self {
            case .desktopAddress: return "Desktop Address"
            case .drop: return "Drop"
            case .windowSize: return "Window Size"
            case .sameType: return "Same Type"
            case .sensorTimestamp: return "Sensor Timestamp"
            case .audio: return "Audio"
            case .video: return "Video"
            case .pointCloud: return "Point Cloud"
            case .accelerometer: return "Accelerometer"
            case .gyroscope: return "Gyroscope"
            case .magnetometer: return "Magnetometer"
            case .barometer: return "Barometer"
            case .stepCounter: return "Step Counter"
            case .position: return "Position"
            case .activity: return "Activity"
            case .videoQuality: return "Video Quality"
            case .accelerometerFrequency,.gyroscopeFrequency, .magnetometerFrequency, .barometerFrequency, .activityFrequency,
                 .proximityFrequency, .altimeterFrequency, .accelerometerRawFrequency, .gyroscopeRawFrequency: return "Frequency"
            case .desktopPort: return "Desktop Port"
            case .proximity: return "Proximity"
            case .altimeter: return "Altimeter"
            case .accelerometerRaw: return "Accelerometer Raw"
            case .gyroscopeRaw: return "Gyroscope Raw"
        }
    }
    var help: String {
        switch self {
            case .desktopAddress: return "Insert the IP address and the port of the desktop receiver, insert only adress omitting the protocol.\ne.g. 127.0.0.1"
            case .desktopPort: return "Insert the port number of the desktop receiver.\n5000"
            case .drop: return "Accept to drop data to guarantee a short delay."
            case .windowSize: return "Time window in which data is collected before being sent."
            case .sameType: return "There may be data of the same type in the same window."
            case .sensorTimestamp: return "Insert a timestamp for each sensor or use the window timestamp."
            case .audio: return "Record audio from microphone."
            case .video: return "Record video from camera."
            case .pointCloud: return "Point Cloud data, a collection of points in the world coordinate space of the AR session."
            case .accelerometer: return "Take data from accelerometer, acceleration force data for the three coordinate axes."
            case .gyroscope: return "Take data from gyroscope, rate of rotation data for the three coordinate axes."
            case .magnetometer: return "Take data from magnetometer, indicating the device's orientation relative to Earth's magnetic field."
            case .barometer: return "Take data from barometer, measures the ambient air pressure."
            case .stepCounter: return "The number of steps taken by the user."
            case .position: return "The most recently retrieved user location."
            case .activity: return "User activity data reflects whether the user is walking, running, in a vehicle, or stationary for periods of time."
            case .videoQuality: return "Quality of the video to be transmitted."
            case .accelerometerFrequency, .activityFrequency, .barometerFrequency,
                 .magnetometerFrequency, .gyroscopeFrequency, .proximityFrequency,
                 .altimeterFrequency, .accelerometerRawFrequency, .gyroscopeRawFrequency: return "Sampling frequency."
            case .proximity: return "Proximity."
            case .altimeter: return "Altimter."
            case .accelerometerRaw: return "Take raw data from accelerometer, acceleration force data for the three coordinate axes."
            case .gyroscopeRaw: return "Take raw data from gyroscope, rate of rotation data for the three coordinate axes."
        }
    }
    var placeholder: String? {
        switch self {
            case .desktopAddress: return "Address"
            case .desktopPort: return "Port"
            default: return nil
        }
    }
    var textType: UITextContentType? {
        switch self {
            case .desktopAddress: return .URL
            case .desktopPort: return .telephoneNumber
            default: return nil
        }
    }
    var pickerOptionData: [PickerOption]? {
        switch self {
            case .windowSize:
                return [PickerOption(5, "5 ms"),
                        PickerOption(10, "10 ms"),
                        PickerOption(15, "15 ms"),
                        PickerOption(20, "20 ms"),
                        PickerOption(25, "25 ms"),
                        PickerOption(30, "30 ms"),
                        PickerOption(35, "35 ms"),
                        PickerOption(40, "40 ms"),
                        PickerOption(45, "45 ms"),
                        PickerOption(50, "50 ms"),
                        PickerOption(55, "55 ms"),
                        PickerOption(60, "60 ms"),
                        PickerOption(65, "65 ms"),
                        PickerOption(70, "70 ms"),
                        PickerOption(75, "75 ms"),
                        PickerOption(80, "80 ms"),
                        PickerOption(85, "85 ms"),
                        PickerOption(90, "90 ms"),
                        PickerOption(95, "95 ms"),
                        PickerOption(100, "100 ms"),
                        PickerOption(110, "110 ms"),
                        PickerOption(120, "120 ms"),
                        PickerOption(130, "130 ms"),
                        PickerOption(140, "140 ms"),
                        PickerOption(150, "150 ms"),
                        PickerOption(160, "160 ms"),
                        PickerOption(170, "170 ms"),
                        PickerOption(180, "180 ms"),
                        PickerOption(190, "190 ms"),
                        PickerOption(200, "200 ms"),
                        PickerOption(300, "300 ms"),
                        PickerOption(400, "400 ms"),
                        PickerOption(500, "500 ms"),
                        PickerOption(600, "600 ms"),
                        PickerOption(700, "700 ms"),
                        PickerOption(800, "800 ms"),
                        PickerOption(900, "900 ms"),
                        PickerOption(1000, "1 sec"),
                        PickerOption(2000, "2 sec"),
                        PickerOption(3000, "3 sec"),
                        PickerOption(4000, "4 sec"),
                        PickerOption(5000, "5 sec"),
                        PickerOption(6000, "6 sec"),
                        PickerOption(7000, "7 sec"),
                        PickerOption(8000, "8 sec"),
                        PickerOption(9000, "9 sec"),
                        PickerOption(10000, "10 sec")]
            case .videoQuality:
                return [PickerOption(144, "144p"),
                        PickerOption(240, "240p"),
                        PickerOption(360, "360p"),
                        PickerOption(480, "480p"),
                        PickerOption(720, "720p"),
                        PickerOption(1080, "1080p")]
            case .accelerometerFrequency, .barometerFrequency, .magnetometerFrequency,
                 .gyroscopeFrequency, .activityFrequency, .altimeterFrequency, .proximityFrequency, .accelerometerRawFrequency, .gyroscopeRawFrequency:
                return [PickerOption(0.01, "0.01 sec"),
                        PickerOption(0.03, "0.05 sec"),
                        PickerOption(0.07, "0.07 sec"),
                        PickerOption(0.1, "0.1 sec"),
                        PickerOption(0.2, "0.2 sec"),
                        PickerOption(0.3, "0.3 sec"),
                        PickerOption(0.4, "0.4 sec"),
                        PickerOption(0.5, "0.5 sec"),
                        PickerOption(0.6, "0.6 sec"),
                        PickerOption(0.7, "0.7 sec"),
                        PickerOption(0.8, "0.8 sec"),
                        PickerOption(0.9, "0.9 sec"),
                        PickerOption(1, "1 sec"),
                        PickerOption(1.5, "1.5 sec"),
                        PickerOption(2, "2 sec"),
                        PickerOption(3, "3 sec"),
                        PickerOption(4, "4 sec"),
                        PickerOption(5, "5 sec"),
                        PickerOption(7, "7 sec"),
                        PickerOption(10, "10 sec"),
                        PickerOption(15, "15 sec"),
                        PickerOption(20, "20 sec"),
                        PickerOption(30, "30 sec"),
                        PickerOption(50, "50 sec"),
                        PickerOption(60, "1 min"),
                        PickerOption(90, "1.5 min"),
                        PickerOption(120, "2 min")]
            default: return nil
        }
    }

    var isAvailable: Bool {
        switch self {
            case .audio:
                let audioDevice = AVCaptureDevice.default(.builtInMicrophone,
                                                          for: .audio,
                                                          position: .unspecified)
                return audioDevice != nil
            case .video:
                let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                          for: .video,
                                                          position: .back)
                return videoDevice != nil
            case .pointCloud:
                return ARConfiguration.isSupported
            case .accelerometer, .accelerometerRaw:
                return CMMotionManager().isAccelerometerAvailable
            case .gyroscope, .gyroscopeRaw:
                return CMMotionManager().isGyroAvailable
            case .magnetometer:
                return CMMotionManager().isMagnetometerAvailable
            case .barometer, .altimeter:
                return CMAltimeter.isRelativeAltitudeAvailable()
            case .stepCounter:
                return CMPedometer.isStepCountingAvailable()
            case .position:
                return CLLocationManager().location != nil
            case .activity:
                return CMMotionActivityManager.isActivityAvailable()
            case .proximity:
                return UIDevice().isProximityMonitoringEnabled
            default: return false
        }
    }
}


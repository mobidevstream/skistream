# Requirements
```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

# Start desktop
```
$ source venv/bin/activate
$ python desktop.py
```

# Stop desktop
```
Press CTRL+C to quit
$ deactivate
```

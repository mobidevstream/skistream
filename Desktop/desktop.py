import datetime
import json

from flask import Flask, request, abort
from google.protobuf.json_format import MessageToJson

from proto import window_pb2

app = Flask(__name__)

types = {
    1: "Audio",
    2: "Video",
    3: "Point Cloud",
    4: "Accelerometer",
    5: "Gyroscope",
    6: "Magnetometer",
    7: "Barometer",
    8: "Step Counter",
    9: "Position",
    10: "Activity"
}


def str_timestamp(timestamp):
    date = str(datetime.datetime.fromtimestamp(int(timestamp) / 1000.0))
    return date[:-3]


@app.route('/')
def index():
    return "SKIstream Desktop"


@app.route('/json', methods=['POST'])
def post_json():
    if not request.json or not 'type' in request.json:
        abort(400)
    print(types[request.json['type']])
    for field in request.json:
        if field != 'type':
            print(request.json[field])
    return '', 200


@app.route('/stream', methods=['POST'])
def post_protoc():
    if not request.data:
        abort(400)
    window = window_pb2.Window()
    window.ParseFromString(request.data)
    print("NEW WINDOW: \n")

    x = json.loads(MessageToJson(window))
    for key in x:
        value = x[key]
        print(key, end=': ')
        if key == "timestamp":
            print(str_timestamp(value))
        elif isinstance(value, list):
            print("[")
            for y in value:
                print("\t{", end='')
                for k, v in y.items():
                    print("\n\t\t" + k + ": ", end=' ')
                    if k == "timestamp":
                        print(str_timestamp(v), end='')
                    else:
                        print(str(v), end='')
                print("\n\t}")
            print("]")
        else:
            print(str(value))
    return '', 200


if __name__ == '__main__':
    print("Insert port number:")
    isInt = False
    while not isInt:
        userInput = input()
        try:
            port = int(userInput)
            isInt = True
        except ValueError:
            print("Insert an int value!")
    app.run(host='0.0.0.0', port=port)

import requests

from proto import window_pb2

SERVER_ADDRESS = "http://localhost:"
port = 0


def send_json():
    r = requests.post(SERVER_ADDRESS + str(port) + "/json",
                      json={"type": 4, "x": 12, "y": 67, "z": 987, "values": [19, 28, 75]})
    print(r.status_code, r.reason, "\n")


def send_proto():
    window = window_pb2.Window()
    window.timestamp = 1789
    a1 = window_pb2.Accelerometer()
    a1.x = 9
    a1.y = 12
    a1.z = 124
    a1.timestamp = 198
    a2 = window_pb2.Accelerometer()
    a2.x = 9
    a2.y = 12
    a2.z = 124
    a2.timestamp = 198

    window.accelerometer.append(a1)
    window.accelerometer.append(a2)
    r = requests.post(SERVER_ADDRESS + str(port) + "/stream", data=window.SerializeToString())
    print(r.status_code, r.reason, "\n")


if __name__ == "__main__":
    print("Insert port number:")
    isInt = False
    while not isInt:
        userInput = input()
        try:
            port = int(userInput)
            isInt = True
        except ValueError:
            print("Insert an int value!")
    print("0: exit\n"
          "1: json\n"
          "2: protocol buffers\n")
    n = int(input())
    while n != 0:
        if n == 1:
            send_json()
        elif n == 2:
            send_proto()
        n = int(input())
    exit()
